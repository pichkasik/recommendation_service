package com.epam.recommendation.models.dto;

import lombok.Builder;
import lombok.Value;

import java.util.Date;
import java.util.List;

@Value
@Builder
public class CryptoResponse {
  Date startDate;
  Date endDate;
  List<CoinStatistic> data;

}
