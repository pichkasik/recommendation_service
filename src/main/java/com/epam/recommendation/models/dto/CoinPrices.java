package com.epam.recommendation.models.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class CoinPrices {
  String coinName;
  BigDecimal minPrice;
  BigDecimal maxPrice;
  BigDecimal oldestPrice;
  BigDecimal newestPrice;
}
