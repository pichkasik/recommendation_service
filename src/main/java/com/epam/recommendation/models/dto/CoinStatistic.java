package com.epam.recommendation.models.dto;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class CoinStatistic {
  String coinName;
  BigDecimal minPrice;
  BigDecimal maxPrice;
}
