package com.epam.recommendation.models.dto;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class NormalizedRangeRecord {
  String coinName;
  BigDecimal normalizedRange;
}
