package com.epam.recommendation.models.dto;

import lombok.Builder;
import lombok.Value;

import java.util.Date;

@Value
@Builder
public class StatisticResponse {
  Date startDate;
  Date endDate;
  CoinPrices prices;
}
