package com.epam.recommendation.models.dto;

import lombok.Builder;
import lombok.Value;

import java.util.Date;
import java.util.List;

@Value
@Builder
public class NormalizedRangeResponse {
  Date startDate;
  Date endDate;
  List<NormalizedRangeRecord> ranges;
}
