package com.epam.recommendation.models.dto;

import com.epam.recommendation.models.validation.ValidationType;
import lombok.Builder;
import lombok.Value;

import java.util.Date;
import java.util.Set;

@Builder
@Value
public class CryptoRequest {
  Date startDate;
  Date endDate;
  String coinName;
  Set<ValidationType> validationTypes;
}
