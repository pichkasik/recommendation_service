package com.epam.recommendation.models.validation;

public enum ValidationType {
  VALIDATE_DATE,
  VALIDATE_COIN_NAME
}
