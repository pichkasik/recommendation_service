package com.epam.recommendation.controllers.handler;

import com.epam.recommendation.exceptions.DateException;
import com.epam.recommendation.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionHandler {

  @ExceptionHandler({DateException.class})
  public ResponseEntity<String> badRequestHandler(final Exception ex) {
    String message = ex.getMessage();
    return new ResponseEntity<String>(message, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler({NotFoundException.class})
  public ResponseEntity<String> notFoundRequestHandler(final Exception ex) {
    String message = ex.getMessage();
    return new ResponseEntity<String>(message, HttpStatus.NOT_FOUND);
  }
}
