package com.epam.recommendation.controllers;

import com.epam.recommendation.constants.DateConstants;
import com.epam.recommendation.exceptions.DateException;
import com.epam.recommendation.exceptions.NotFoundException;
import com.epam.recommendation.models.dto.CryptoRequest;
import com.epam.recommendation.models.dto.NormalizedRangeResponse;
import com.epam.recommendation.models.dto.StatisticResponse;
import com.epam.recommendation.models.validation.ValidationType;
import com.epam.recommendation.services.CryptoService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/recommendation")
public class CryptoController {

  private final CryptoService cryptoService;

  //TODO document with swagger
  //TODO add custom deserializer to make the Date formatting common

  @GetMapping("/comparing")
  public NormalizedRangeResponse getComparing(@RequestParam @DateTimeFormat(pattern = DateConstants.datePattern) Date startDate,
                                              @RequestParam @DateTimeFormat(pattern = DateConstants.datePattern) Date endDate) throws DateException, NotFoundException {
    CryptoRequest request = CryptoRequest.builder()
            .startDate(startDate)
            .endDate(endDate)
            .validationTypes(Set.of(ValidationType.VALIDATE_DATE))
            .build();
    return cryptoService.getComparing(request);
  }

  @GetMapping("/info")
  public StatisticResponse getCrypto(@RequestParam String coin,
                                     @RequestParam @DateTimeFormat(pattern = DateConstants.datePattern) Date startDate,
                                     @RequestParam @DateTimeFormat(pattern = DateConstants.datePattern) Date endDate) throws DateException, NotFoundException {
    CryptoRequest request = CryptoRequest.builder()
            .coinName(coin)
            .startDate(startDate)
            .endDate(endDate)
            .validationTypes(Set.of(ValidationType.VALIDATE_DATE, ValidationType.VALIDATE_COIN_NAME))
            .build();
    return cryptoService.getCrypto(request);
  }

  @GetMapping("/top")
  public NormalizedRangeResponse getTop(@RequestParam @DateTimeFormat(pattern = DateConstants.datePattern) Date day) throws DateException, NotFoundException {
    return cryptoService.getTop(day);
  }
}
