package com.epam.recommendation.controllers;

import com.epam.recommendation.services.UploadService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/upload")
public class UploadController {

  private final UploadService uploadService;

  @PostMapping
  public String uploadCsv(@RequestParam MultipartFile file) throws IOException {
    return uploadService.upload(file);
  }
}
