package com.epam.recommendation.repositories;

import com.epam.recommendation.models.dto.CoinStatistic;
import com.epam.recommendation.models.entities.Price;
import jakarta.persistence.Tuple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CryptoRepository extends JpaRepository<Price, Long> {

  @Query("SELECT new com.epam.recommendation.models.dto.CoinStatistic(t.symbol, " +
          "MAX(t.price), MIN(t.price)) " +
          "FROM Price t " +
          "WHERE t.timestamp BETWEEN  :startDate AND :endDate " +
          "GROUP BY t.symbol")
  List<CoinStatistic> getAllMinMax(@Param("startDate") Long startDate, @Param("endDate") Long endDate);

  //TODO rewrite query to retrieve a DTO instead of Tuple
  @Query(nativeQuery = true, value = "SELECT p.symbol, first(p.price, p.timestamp), last(p.price, p.timestamp), max(p.price), min(p.price) " +
          "FROM t_price p WHERE p.symbol = :coinName AND p.timestamp BETWEEN :startDate AND :endDate " +
          "GROUP BY p.symbol " +
          "ORDER BY p.symbol")
  Tuple getOldestAndNewestPrices(@Param("coinName") String coinName, @Param("startDate") Long startDate, @Param("endDate") Long endDate);
}
