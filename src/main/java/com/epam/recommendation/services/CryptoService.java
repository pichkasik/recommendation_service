package com.epam.recommendation.services;

import com.epam.recommendation.exceptions.DateException;
import com.epam.recommendation.exceptions.NotFoundException;
import com.epam.recommendation.models.dto.CryptoRequest;
import com.epam.recommendation.models.dto.NormalizedRangeResponse;
import com.epam.recommendation.models.dto.StatisticResponse;

import java.util.Date;

public interface CryptoService {
  NormalizedRangeResponse getComparing(CryptoRequest comparingRequest) throws DateException, NotFoundException;

  StatisticResponse getCrypto(CryptoRequest cryptoRequest) throws DateException, NotFoundException;

  NormalizedRangeResponse getTop(Date day) throws DateException, NotFoundException;



}
