package com.epam.recommendation.services.impl;

import com.epam.recommendation.models.entities.Price;
import com.epam.recommendation.repositories.CryptoRepository;
import com.epam.recommendation.services.UploadService;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.list.PredicatedList;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UploadServiceImpl implements UploadService {

  private final CryptoRepository cryptoRepository;

  //TODO create Response model
  @Override
  public String upload(MultipartFile file) throws IOException {
    Reader reader = new InputStreamReader(file.getInputStream());
    List<Price> prices = new CsvToBeanBuilder<Price>(reader)
            .withType(Price.class)
            .withSeparator(',')
            .withIgnoreLeadingWhiteSpace(true)
            .build()
            .parse();
    cryptoRepository.saveAll(prices);
    return "Cool";
  }
}
