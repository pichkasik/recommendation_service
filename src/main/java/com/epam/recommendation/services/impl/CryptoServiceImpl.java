package com.epam.recommendation.services.impl;

import com.epam.recommendation.exceptions.DateException;
import com.epam.recommendation.exceptions.NotFoundException;
import com.epam.recommendation.models.dto.CoinPrices;
import com.epam.recommendation.models.dto.CoinStatistic;
import com.epam.recommendation.models.dto.CryptoRequest;
import com.epam.recommendation.models.dto.NormalizedRangeRecord;
import com.epam.recommendation.models.dto.NormalizedRangeResponse;
import com.epam.recommendation.models.dto.StatisticResponse;
import com.epam.recommendation.models.validation.ValidationType;
import com.epam.recommendation.repositories.CryptoRepository;
import com.epam.recommendation.services.CryptoService;
import jakarta.persistence.Tuple;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class CryptoServiceImpl implements CryptoService {

  private final CryptoRepository cryptoRepository;
  private final static Integer MAX_COIN_NAME_LENGTH = 5;

  @Override
  public NormalizedRangeResponse getComparing(CryptoRequest comparingRequest) throws DateException, NotFoundException {
    validateRequest(comparingRequest);
    List<CoinStatistic> coinStatistics = cryptoRepository
            .getAllMinMax(comparingRequest.getStartDate().getTime(), comparingRequest.getEndDate().getTime());
    List<NormalizedRangeRecord> normalizedRanges = getNormalizedRanges(coinStatistics);

    return NormalizedRangeResponse.builder()
            .startDate(comparingRequest.getStartDate())
            .endDate(comparingRequest.getEndDate())
            .ranges(normalizedRanges)
            .build();
  }

  @Override
  public StatisticResponse getCrypto(CryptoRequest cryptoRequest) throws DateException, NotFoundException {
    validateRequest(cryptoRequest);
    //TODO validate if the coin is not exist in our system via cache
    //TODO get rid of Tuple in Repo change to DTO instead
    Tuple prices = cryptoRepository.getOldestAndNewestPrices(
            cryptoRequest.getCoinName(),
            cryptoRequest.getStartDate().getTime(),
            cryptoRequest.getEndDate().getTime());
      if (Objects.isNull(prices) || prices.getElements().isEmpty()) {
        throw new NotFoundException("The coin '%s' doesn't exist or the there is no available data for the time frame"
                            .formatted(cryptoRequest.getCoinName()));
      }
    //TODO cache the Crypto names
    CoinPrices coinPrices = CoinPrices.builder()
            .coinName(prices.get("symbol", String.class))
            .maxPrice(prices.get("max", BigDecimal.class))
            .minPrice(prices.get("min", BigDecimal.class))
            .newestPrice(prices.get("last", BigDecimal.class))
            .oldestPrice(prices.get("first", BigDecimal.class))
            .build();

    return StatisticResponse.builder()
            .startDate(cryptoRequest.getStartDate())
            .endDate(cryptoRequest.getEndDate())
            .prices(coinPrices)
            .build();
  }

  @Override
  public NormalizedRangeResponse getTop(Date day) throws DateException, NotFoundException {
    CryptoRequest request = CryptoRequest.builder()
            .startDate(getBeginOfDay(day))
            .endDate(getEndOfDay(day))
            .build();
    NormalizedRangeResponse rangeResponse = getComparing(request);
    NormalizedRangeRecord topRecord = rangeResponse.getRanges().stream()
            .findFirst()
            .orElseThrow(() -> new NotFoundException("There is no prices for the day: %s".formatted(day)));

    return NormalizedRangeResponse.builder()
            .startDate(rangeResponse.getStartDate())
            .endDate(rangeResponse.getEndDate())
            .ranges(List.of(topRecord))
            .build();
  }


  private Date getBeginOfDay(Date day) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(day);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    return calendar.getTime();
  }

  private Date getEndOfDay(Date day) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(day);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
    calendar.set(Calendar.MILLISECOND, 999);
    return calendar.getTime();
  }

  private void validateRequest(CryptoRequest comparingRequest) throws NotFoundException, DateException {
    //TODO clean the mess
    if (Objects.isNull(comparingRequest.getValidationTypes())) {
      return;
    }
    for (ValidationType type : comparingRequest.getValidationTypes()) {
      if (type.equals(ValidationType.VALIDATE_DATE)) {
        if (comparingRequest.getStartDate().after(comparingRequest.getEndDate())) {
          throw new DateException("The startDate has to be earlier then end date");
        }
        long daysBetween = ChronoUnit.DAYS.between(comparingRequest.getStartDate().toInstant(), comparingRequest.getEndDate().toInstant());
        if (daysBetween > 365) {
          throw new DateException("We don't support the timeframe more then one year. Please call to support)");
        }
      }

      if (type.equals(ValidationType.VALIDATE_COIN_NAME)) {
        if (Objects.isNull(comparingRequest.getCoinName())
                || comparingRequest.getCoinName().length() > MAX_COIN_NAME_LENGTH) {
          throw new NotFoundException("Coin name can't be more than: %s symbols".formatted(MAX_COIN_NAME_LENGTH));
        }
      }
    }
  }

  private List<NormalizedRangeRecord> getNormalizedRanges(final List<CoinStatistic> statistics) {
    return statistics.stream()
            .map(this::mapNormalizedRangeRecord)
            .sorted(Comparator.comparing(NormalizedRangeRecord::getNormalizedRange).reversed())
            .toList();
  }

  private NormalizedRangeRecord mapNormalizedRangeRecord(final CoinStatistic statistic) {
    return NormalizedRangeRecord.builder()
            .coinName(statistic.getCoinName())
            .normalizedRange(countNormalizedRange(statistic.getMaxPrice(), statistic.getMinPrice()))
            .build();
  }

  private BigDecimal countNormalizedRange(BigDecimal max, BigDecimal min) {
    return max.subtract(min).divide(max, 2, RoundingMode.HALF_UP);
  }
}
