package com.epam.recommendation.exceptions;

public class DateException extends Exception {
  public DateException(String message) {
    super(message);
  }
}
