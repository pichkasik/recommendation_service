package com.epam.recommendation.exceptions;

public class NotFoundException extends Exception {
  public NotFoundException(String message) {
    super(message);
  }
}
