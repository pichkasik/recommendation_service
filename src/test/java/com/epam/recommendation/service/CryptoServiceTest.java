package com.epam.recommendation.service;


import com.epam.recommendation.constants.DateConstants;
import com.epam.recommendation.exceptions.DateException;
import com.epam.recommendation.exceptions.NotFoundException;
import com.epam.recommendation.models.dto.CoinStatistic;
import com.epam.recommendation.models.dto.CryptoRequest;
import com.epam.recommendation.models.dto.NormalizedRangeResponse;
import com.epam.recommendation.models.dto.StatisticResponse;
import com.epam.recommendation.models.validation.ValidationType;
import com.epam.recommendation.repositories.CryptoRepository;
import com.epam.recommendation.services.CryptoService;
import jakarta.persistence.Tuple;
import org.hibernate.jpa.spi.NativeQueryTupleTransformer;
import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class CryptoServiceTest {

  @Autowired
  private CryptoService cryptoService;
  @MockBean
  private CryptoRepository cryptoRepository;
  private final EasyRandom easyRandom = new EasyRandom();

  private final SimpleDateFormat dateFormat = new SimpleDateFormat(DateConstants.datePattern);
  private Date startDate;
  private Date endDate;

  @Configuration
  @ComponentScan(basePackages = "com.epam.recommendation.services")
  static class ContextConfiguration {

  }

  @BeforeEach
  void setUp() throws ParseException {
    startDate = dateFormat.parse("01-15-2023");
    endDate = dateFormat.parse("02-15-2023");
  }

  @Test
  void comparingSuccessTest() throws ParseException, DateException, NotFoundException {
    List<CoinStatistic> coinStatistics = createCoinStatistics();
    when(cryptoRepository.getAllMinMax(any(), any())).thenReturn(coinStatistics);
    CryptoRequest cryptoRequest = CryptoRequest.builder()
            .startDate(startDate)
            .endDate(endDate)
            .build();

    NormalizedRangeResponse comparing = cryptoService.getComparing(cryptoRequest);

    assertThat(comparing.getRanges().size()).isEqualTo(coinStatistics.size());
    assertThat(comparing.getStartDate()).isNotNull();
    assertThat(comparing.getStartDate()).isNotNull();
  }

  @Test
  void getCryptoSuccessTest() throws DateException, NotFoundException {
    String coinName = "BTC";
    Tuple prices = createPrices();
    when(cryptoRepository.getOldestAndNewestPrices(anyString(), any(), any())).thenReturn(prices);
    CryptoRequest cryptoRequest = CryptoRequest.builder()
            .coinName(coinName)
            .startDate(startDate)
            .endDate(endDate)
            .build();

    StatisticResponse crypto = cryptoService.getCrypto(cryptoRequest);

    assertThat(crypto.getPrices().getCoinName()).isEqualTo(coinName);
    assertThat(crypto.getStartDate()).isEqualTo(startDate);
    assertThat(crypto.getEndDate()).isEqualTo(endDate);
  }

  @Test
  void getTopSuccessTest() throws DateException, NotFoundException, ParseException {
    List<CoinStatistic> coinStatistics = createCoinStatistics();
    when(cryptoRepository.getAllMinMax(any(), any())).thenReturn(coinStatistics);

    NormalizedRangeResponse top = cryptoService.getTop(startDate);

    assertThat(top.getRanges().size()).isEqualTo(1);
    assertThat(top.getStartDate()).isEqualTo(startDate);
  }

  @Test
  void comparingMustThrowDateExceptionTest() throws ParseException, DateException, NotFoundException {
    Date startMoreThenOneYearAgo = dateFormat.parse("01-15-2020");
    Date endDate =  dateFormat.parse("01-15-2023");
    CryptoRequest cryptoRequest = CryptoRequest.builder()
            .startDate(startMoreThenOneYearAgo)
            .endDate(endDate)
            .validationTypes(Set.of(ValidationType.VALIDATE_DATE))
            .build();

    assertThatThrownBy(
            () -> cryptoService.getComparing(cryptoRequest)).isInstanceOf(DateException.class);
  }

  @Test
  void getCryptoMustThrowNotFoundCoinNameExceptionTest() {
    CryptoRequest cryptoRequest = CryptoRequest.builder()
            .coinName("not-exist-coin-name")
            .startDate(startDate)
            .endDate(endDate)
            .validationTypes(Set.of(ValidationType.VALIDATE_COIN_NAME))
            .build();

    assertThatThrownBy(
            () -> cryptoService.getCrypto(cryptoRequest)).isInstanceOf(NotFoundException.class);

  }

  private List<CoinStatistic> createCoinStatistics() {
    return easyRandom.objects(CoinStatistic.class, 5).toList();
  }

  private Tuple createPrices() {
    Tuple t = new NativeQueryTupleTransformer().transformTuple(new Object[]{
            "BTC",
            new BigDecimal("50000.00"),
            new BigDecimal("45000.00"),
            new BigDecimal("49000.00"),
            new BigDecimal("47000.00"),
    }, new String[]{"symbol",
            "max",
            "min",
            "last",
            "first"});
    return t;
  }
}
