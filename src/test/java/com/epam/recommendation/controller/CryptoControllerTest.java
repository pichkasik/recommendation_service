package com.epam.recommendation.controller;

import com.epam.recommendation.controllers.CryptoController;
import com.epam.recommendation.exceptions.DateException;
import com.epam.recommendation.exceptions.NotFoundException;
import com.epam.recommendation.models.dto.CoinPrices;
import com.epam.recommendation.models.dto.NormalizedRangeResponse;
import com.epam.recommendation.models.dto.StatisticResponse;
import com.epam.recommendation.services.CryptoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.math.BigDecimal;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = CryptoController.class)
class CryptoControllerTest {

  @Autowired
  MockMvc mockMvc;
  @Autowired
  ObjectMapper mapper;

  @MockBean
  private CryptoService cryptoService;
  private MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();

  @BeforeEach
  void setUp() {
    queryParams.add("startDate", "01-15-2023");
    queryParams.add("endDate", "02-15-2023");
  }

  @AfterEach
  void clear() {
    queryParams.clear();
  }

  @Test
  void comparingTest() throws Exception {
    NormalizedRangeResponse serviceResponse = createNormalizedRangeResponse();
    when(cryptoService.getComparing(any())).thenReturn(serviceResponse);

    String responseBodyAsString = mockMvc.perform(get("/v1/recommendation/comparing")
                    .contentType(MediaType.APPLICATION_JSON)
                    .queryParams(queryParams))
            .andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

    NormalizedRangeResponse controllerResponse = mapper.readValue(responseBodyAsString, NormalizedRangeResponse.class);
    assertThat(controllerResponse.getRanges().size()).isEqualTo(serviceResponse.getRanges().size());
  }

  @Test
  void getCryptoTest() throws Exception {
    String coinName = "BTC";
    StatisticResponse serviceResponse = createStatisticResponse(coinName);
    when(cryptoService.getCrypto(any())).thenReturn(serviceResponse);
    queryParams.add("coin", coinName);

    String responseBodyAsString = mockMvc.perform(get("/v1/recommendation/info")
                    .contentType(MediaType.APPLICATION_JSON)
                    .queryParams(queryParams))
            .andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

    StatisticResponse controllerResponse = mapper.readValue(responseBodyAsString, StatisticResponse.class);
    assertThat(controllerResponse.getPrices()).isEqualTo(serviceResponse.getPrices());
  }

  @Test
  void getTopTest() throws Exception {
    NormalizedRangeResponse serviceResponse = createNormalizedRangeResponse();
    when(cryptoService.getTop(any())).thenReturn(serviceResponse);

    String responseBodyAsString = mockMvc.perform(get("/v1/recommendation/top")
                    .contentType(MediaType.APPLICATION_JSON)
                    .queryParam("day", "01-15-2023"))
            .andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

    NormalizedRangeResponse controllerResponse = mapper.readValue(responseBodyAsString, NormalizedRangeResponse.class);
    assertThat(controllerResponse.getRanges().size()).isEqualTo(serviceResponse.getRanges().size());
  }

  @Test
  void comparingThrowsDateException() throws Exception {
    when(cryptoService.getComparing(any())).thenThrow(DateException.class);

    mockMvc.perform(get("/v1/recommendation/comparing")
                    .contentType(MediaType.APPLICATION_JSON)
                    .queryParams(queryParams))
            .andExpect(status().isBadRequest());
  }

  @Test
  void getCryptoThrowsNotFoundCoinNameException() throws Exception {
    when(cryptoService.getCrypto(any())).thenThrow(NotFoundException.class);
    queryParams.add("coin", "non-exist-coin");

    mockMvc.perform(get("/v1/recommendation/info")
                    .contentType(MediaType.APPLICATION_JSON)
                    .queryParams(queryParams))
            .andExpect(status().isNotFound());
  }

  private NormalizedRangeResponse createNormalizedRangeResponse() {
    EasyRandomParameters parameters = new EasyRandomParameters();
    parameters.collectionSizeRange(5, 5);
    EasyRandom easyRandom = new EasyRandom(parameters);
    return easyRandom.nextObject(NormalizedRangeResponse.class);
  }

  private StatisticResponse createStatisticResponse(String coinName) {
    EasyRandom easyRandom = new EasyRandom();
    CoinPrices coinStatistic = CoinPrices.builder()
            .coinName(coinName)
            .maxPrice(BigDecimal.valueOf(easyRandom.nextDouble()))
            .minPrice(BigDecimal.valueOf(easyRandom.nextDouble()))
            .oldestPrice(BigDecimal.valueOf(easyRandom.nextDouble()))
            .newestPrice(BigDecimal.valueOf(easyRandom.nextDouble()))
            .build();
    return StatisticResponse.builder()
            .startDate(easyRandom.nextObject(Date.class))
            .endDate(easyRandom.nextObject(Date.class))
            .prices(coinStatistic)
            .build();
  }
}
